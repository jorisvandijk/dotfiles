[[ $- != *i* ]] && return
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100

BLUE="\[\e[1;34m\]"
COMMAND_COUNT=0
GREEN="\[\e[1;32m\]"
GREY="\[\e[0;37m\]"
PATH="$HOME/.local/bin:${PATH:+:${PATH}}"
RESET="\[\e[0m\]"

alias ..='cd ..'
alias @='jrnl' 
alias @?='jrnl -to today'
alias s='jrnl school' 
alias s?='jrnl school -to today'
alias 11='npx @11ty/eleventy --serve'
alias c='COMMAND_COUNT=0; clear && printf "\033[3J"'
alias checkout='git checkout'
alias clean='sudo pacman -Qtdq | sudo pacman -Rns -'
alias fetch='git fetch'
alias grep='grep --color=auto'
alias h='SearchHistory'
alias kali='distrobox enter --root kali'
alias la='exa -lab --header --color=always --group-directories-first --long --git'
alias ls='exa -lb --header --color=always --group-directories-first --long --git'
alias m='micro'
alias mc='make clean'
alias mt='make test'
alias mp3='yt-dlp --extract-audio --audio-format mp3 '
alias n='newsboat'
alias nopwn='docker-compose down'
alias p='pulsemixer'
alias pwn='cd ~/git/docker && docker-compose up -d'
alias pull='git pull --rebase'
alias push='Push'
alias re='ssh root@192.168.10.43'
alias server='ssh root@10.10.10.10'
alias space='ncdu'
alias status='git status'
alias tape='ScreenRecorder'
alias u='yay -Syyu --noconfirm'
alias v='nvim'
alias w='curl wttr.in'
alias x='chmod +x'
alias yt='mpv --ytdl-format="bestvideo+bestaudio/best" '

bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'
bind '"\C-g":"cursor . >/dev/null 2>&1 & kitty @ close-window\n"'

export CM_LAUNCHER=Dmenu
export CM_HISTLENGTH=20
export EDITOR=micro
export GIT_PS1_SHOWDIRTYSTATE=1
export HISTCONTROL=ignoredups
export HISTFILESIZE=100000
export HISTSIZE=100000
export HISTFILE=~/.bash_history
export MICRO_TRUECOLOR=1
export MOZ_USE_XINPUT_2=1
export PATH=$HOME/.bin::$PATH
export PROMPT_COMMAND='history -a'
export TERM=kitty
export TERMINAL=kitty
export VISUAL='kitty -e micro'

export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache
export HISTFILE="${XDG_STATE_HOME}"/bash/history
export XINITRC="$XDG_CONFIG_HOME"/x11/xinitrc
export XAUTHORITY="$XDG_RUNTIME_DIR"/.Xauthority

function add_newline_after_command() {
    if [[ $COMMAND_COUNT -gt 0 ]]; then
        echo ""
    fi
    (( COMMAND_COUNT++ ))
}

function cd() { 
  builtin cd "$@" && exa -lb --header --color=always --group-directories-first --long --git --icons
}

function ex () {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)   tar xvjf $1    ;;
           *.tar.gz)    tar xvzf $1    ;;
           *.bz2)       bunzip2 $1     ;;
           *.rar)       unrar x $1     ;;
           *.gz)        gunzip $1      ;;
           *.tar)       tar xvf $1     ;;
           *.tbz2)      tar xvjf $1    ;;
           *.tgz)       tar xvzf $1    ;;
           *.zip)       unzip $1       ;;
           *.Z)         uncompress $1  ;;
           *.7z)        7z x $1        ;;
           *.xz)        unxz $1        ;;
           *)           echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
}

function o() {
    selected_file=$(find "$HOME" -path "$HOME/.local/share/containers" -prune -o -type f -print | fzf)
    if [ -n "$selected_file" ]; then
        file_extension="${selected_file##*.}"
        command=""
        case "$file_extension" in
            pdf)
                command="firefox \"$selected_file\""
                ;;
            jpg|jpeg|png|gif)
                command="feh \"$selected_file\""
                ;;
            mp4|mkv|avi|mp3|wav)
                command="mpv \"$selected_file\""
                ;;
            html|htm)
                command="firefox \"$selected_file\""
                ;;
            txt|md|log|sh|*)
                command="micro \"$selected_file\""
                ;;
            h|c|cpp)
                command="code \"$selected_file\""
                ;;
            *)
                command="xdg-open \"$selected_file\""
                ;;
        esac
        if [ -n "$command" ]; then
            echo "$command" >> ~/.bash_history
            eval "$command"
        fi
    fi
}

function O() {
    selected_dir=$(find "$HOME" -path "$HOME/.local/share/containers" -prune -o -type d -print | fzf)
    if [[ -n "$selected_dir" ]]; then
        command="cd \"$selected_dir\""
        
        echo "$command" >> ~/.bash_history
        eval "$command"
    fi
}

function i() {
    selected_packages=$(yay -Slq | fzf --multi --preview "yay -Si {1}" --preview-window=right:70%)
    if [[ -n "$selected_packages" ]]; then
        command="yay -S $selected_packages"

        echo "$command" >> ~/.bash_history
        eval "$command"
    fi
}

function r() {
    selected_packages=$(pacman -Qq | fzf --multi --preview "pacman -Qi {1}" --preview-window=right:70%)
    if [[ -n "$selected_packages" ]]; then
        command="sudo pacman -Rns $selected_packages"

        echo "$command" >> ~/.bash_history
        eval "$command"
    fi
}

function up() { 
	cd $(eval printf '../'%.0s {1..$1}) && pwd; 
}

function update_ps1() {
    PS1="${GREEN}\w${RESET}"
    PS1+="\n ${BLUE}❯ ${RESET}"
}

shopt -s autocd
shopt -s cdspell
shopt -s checkwinsize
shopt -s histappend

PROMPT_COMMAND='update_ps1; add_newline_after_command'

update_ps1

