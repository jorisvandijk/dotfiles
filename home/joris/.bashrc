#	.bashrc
#
#	By Joris van Dijk | gitlab.com/jorisvandijk 
#	Licensed under the GNU General Public License v3.0

[[ $- != *i* ]] && return
[[ -z "$FUNCNEST" ]] && export FUNCNEST=100

PS1='\n\[\e[1;34m\]\W \[\e[m\]'
PATH="$HOME/.bin:${PATH:+:${PATH}}"

bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
bind 'set completion-ignore-case on'
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'

shopt -s autocd
shopt -s histappend

export CM_LAUNCHER=Jofi
export CM_HISTLENGTH=20
export EDITOR=micro
export HISTCONTROL=erasedups:ignoredups:ignorespace
export HISTFILESIZE=10000
export HISTSIZE=5000
export MICRO_TRUECOLOR=1
export PATH=$HOME/.bin::$PATH
export PROMPT_COMMAND='history -a'
export TERM=kitty
export TERMINAL=kitty
export VISUAL='kitty -e micro'
export MOZ_USE_XINPUT_2=1

alias ..='cd ..'
alias 11='npx @11ty/eleventy --serve'
alias add='git add .'
alias c='clear'
alias clean='sudo pacman -Qtdq | sudo pacman -Rns -'
alias commit='git commit -m'
alias f='ranger'
alias fetch='git fetch'
alias fontys='sudo openconnect vdi.fhict.nl --user=i480134'
alias grep='grep --color=auto'
alias h='sudo openvpn --config $HOME/Documents/HackTheBox/starting_point_jorisvandijk.ovpn'
alias i='yay -Slq | fzf --multi --preview "yay -Si {1}" | xargs -ro yay -S'
alias k='distrobox enter kali'
alias ls='exa -lab --header --color=always --group-directories-first --long --git --icons'  
alias m='micro'
alias n='newsboat'
alias p='pulsemixer'
alias pull='git pull origin'
alias push='git push origin'
alias r='pacman -Qq | fzf --multi --preview "pacman -Qi {1}" | xargs -ro sudo pacman -Rns'
alias re='ssh root@192.168.10.43'
alias resync='rsync -avz -e ssh root@192.168.10.43:/home/root/.local/share/remarkable/xochitl ~/Notes'
alias space='ncdu'
alias status='git status'
alias u='yay -Syyu --noconfirm'
alias v='nvim'
alias w='curl wttr.in'
alias x='chmod +x'

function cd() { # Add exa after a cd
  builtin cd "$@" && exa -lb --header --color=always --group-directories-first --long --git --icons
}

function o(){
	find $HOME/ -type f | fzf | xargs -r $EDITOR
}

function O(){
	find / -type f | fzf 
}

function f(){
	cd $(find $HOME/ -type d | fzf) 
}

eval "$(thefuck --alias)"
